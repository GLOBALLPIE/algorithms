package com.mygdx.project;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g3d.*;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by GlobalPie on 27.11.2018.
 */
public class Screen3D implements Screen {
    private Camera camera;
    private CameraInputController controller;
    private ModelBatch batch;
    CoordRenderer coordRenderer;
    ShapeRenderer shapeRenderer;
    private Vector3 vectorX,vectorY,vectorZ,O;

    public Screen3D() {
        camera=new PerspectiveCamera(67,Main.WIDTH,Main.HEIGHT);
        //camera.position.set(20f,0,20f);
        //camera.lookAt(0f,0f,0f);
        camera.near=0.1f;
        camera.far=8000f;
        O=new Vector3();
        vectorX=Vector3.X.scl(60f);
        vectorY=Vector3.Y.scl(60f);
        vectorZ=Vector3.Z.scl(60f);
        controller=new CameraInputController(camera);
        coordRenderer=new CoordRenderer();
        batch=new ModelBatch();
        shapeRenderer=new ShapeRenderer();
        useCameraTransform(new Vector3(40f,40f,40f),new Vector3(20f,30f,60f),coordRenderer.getTransformMatrix());
        //useCameraTransform(new Vector3(20f,20f,20f),new Vector3(20,-20,80),shapeRenderer.getTransformMatrix());

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(controller);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        camera.update();
        controller.update();
        batch.begin(camera);
        batch.end();
        //Для копирования матрицы используют System.arraycopy()
        coordRenderer.setProjectionMatrix(camera.combined);
        coordRenderer.begin();
        coordRenderer.setColor(Color.GREEN);
        coordRenderer.box(10f,10f,10f,20f,20f,20f);
        coordRenderer.cone(20,-20,80,10,20);
        coordRenderer.drawObjFile("assets/african_head.obj",30f,30f,30f,30f,30f,30f);

        //shapeRenderer.setProjectionMatrix(camera.combined);
       // coordRenderer.begin(ShapeRenderer.ShapeType.Line);
        coordRenderer.setColor(Color.BLUE);
        coordRenderer.line(new Vector3(),vectorX);
        coordRenderer.setColor(Color.GREEN);
        coordRenderer.line(new Vector3(),vectorY);
        coordRenderer.setColor(Color.RED);
        coordRenderer.line(new Vector3(),vectorZ);
        coordRenderer.end();

    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        coordRenderer.dispose();
          batch.dispose();
    }

    public void updateCamera(){
    }

    public void useCameraTransform(Vector3 position, Vector3 lookAt, Matrix4 transformMatrix){
        float[] matrix=transformMatrix.val;
        Vector3 x=new Vector3(matrix[0],matrix[4],matrix[8]).nor();
        Vector3 y=new Vector3(matrix[1],matrix[5],matrix[9]).nor();
        Vector3 z=new Vector3(matrix[2],matrix[6],matrix[10]).nor();
        Vector3 w=new Vector3(position).sub(lookAt).nor();//TODO: Проверить, как работает нормализация
        Vector3 u=new Vector3(w).crs(new Vector3(y).scl(-1)).nor();
        Vector3 v=new Vector3(w).crs(u);
        Vector3 t=new Vector3(matrix[3],matrix[7],matrix[11]).sub(position);
         transformMatrix.mul(new Matrix4(new float[]
                {x.dot(u),x.dot(v),x.dot(w),0f,
                        y.dot(u),y.dot(v),y.dot(w),0f,
                        z.dot(u),z.dot(v),z.dot(w),0f,
                t.dot(u),t.dot(v),t.dot(w),1f}));
    }
}
