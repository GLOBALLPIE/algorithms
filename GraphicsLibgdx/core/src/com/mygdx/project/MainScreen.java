package com.mygdx.project;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FillViewport;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by GlobalPie on 14.10.2018.
 */
public class MainScreen implements Screen {
    public Stage stage;
    public SpriteBatch batch;
    public Texture line;

    public MainScreen(SpriteBatch batch) {
        OrthographicCamera cam = new OrthographicCamera();
        cam.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new FillViewport(Main.WIDTH, Main.HEIGHT, cam), batch);
        //Gdx.gl.glClearColor(27f/255f,29f/255f,30f/255f,1f);
        this.batch = batch;
        Shape shape = new Shape((int) Main.WIDTH, (int) Main.HEIGHT);
       // shape.drawVAShape(new Point2D[]{new Point2D(400,0),new Point2D(0,225),new Point2D(200,250),new Point2D(400,225)},new Point2D[]{new Point2D(200,0),new Point2D(100,10),new Point2D(100,300),new Point2D(300,100),new Point2D(200,150),new Point2D(230,90)},Color.BLACK);
       shape.drawFilledObj("assets/african_head.obj",200,200);
       //shape.drawObj("assets/african_head.obj",200,200);
      //  Pattern pattern=Pattern.compile("v (-?([\\d]|0.[\\d]+)[ ]+)+[-]?([\\d]+|0.[\\d]+)[ ]*");
       // System.out.println(pattern.matcher("v 1 -0.1 -0.1").matches());
      //  System.out.println(Arrays.toString(" 2 2 2  2".trim().split("[ ]+")));
        //System.out.println(Arrays.toString("12/12 12".split("[/ ]")));
        line = new Texture(shape);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
//        Gdx.gl.glClearColor(135/255f, 206/255f, 235/255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
        batch.begin();
        batch.draw(line, 0, 0);
        batch.end();
    }

    @Override
    public void resize(int width, int height) {
        // stage.getViewport().update(width, height,false);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        //batch.dispose();
    }
}
