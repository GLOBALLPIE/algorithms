package com.mygdx.project;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by GlobalPie on 29.11.2018.
 */
public class CoordRenderer extends ShapeRenderer {
    /**
     * Активация кручения
     **/
    private boolean twistX;
    private boolean twistY;
    private boolean twistZ;
    /**
     * Активация изгиба
     */
    private boolean bendX;
    private boolean bendY;
    private boolean bendZ;

    /**
     * Угол для кручения
     *
     * @see #twistX(float)
     **/
    private float angleTwistX;
    private float angleTwistY;
    private float angleTwistZ;

    private float angleBendX;
    private float angleBendY;
    private float angleBendZ;

    public CoordRenderer() {
        super();
        twistX = false;
        twistY = false;
        twistZ = false;
        bendX = false;
        bendY = false;
        bendZ = false;
    }

    public void drawObjFile(String fileName, float x,float y, float z,float scaleX,float scaleY, float scaleZ){
        String str;
        Point point;
        Scanner scanner;

        double[] coords = new double[3];
        int[] indices;
        ArrayList<Point> pointList = new ArrayList();


        try {
            scanner = new Scanner(new File(fileName));//useDelimiter("[#]+[\\w\\W]*\n?");
        }catch (FileNotFoundException f){
            f.printStackTrace();
            System.exit(-1);//TODO: Нужно что-нибудь более изящное
            return;
        }
        while (scanner.hasNextLine()) {//Используем hasNextLine, а не hasNext
            str = scanner.nextLine();
            if (str.startsWith("v ")) {
                coords = Arrays.stream(str.substring(1).trim().split("[ ]+")).mapToDouble(coord -> Double.parseDouble(coord)).toArray();//TODO: Узнать, как работает split()
                point = new Point((int) (coords[0] * scaleX), ((int) (coords[1] * scaleY)),((int) (coords[2] * scaleZ)));
                pointList.add(point);
            }
            if (str.startsWith("f ")) {
                if (pointList.isEmpty()) break;
                indices = Arrays.stream(str.substring(1).trim().split("[/ ]+")).mapToInt(index -> Integer.parseInt(index)).toArray();
                line(pointList.get(indices[0]-1).getX(),pointList.get(indices[0]-1).getY(),pointList.get(indices[0]-1).getZ(),pointList.get(indices[3]-1).getX(),pointList.get(indices[3]-1).getY(),pointList.get(indices[3]-1).getZ());
                line(pointList.get(indices[3]-1).getX(),pointList.get(indices[3]-1).getY(),pointList.get(indices[3]-1).getZ(),pointList.get(indices[6]-1).getX(),pointList.get(indices[6]-1).getY(),pointList.get(indices[6]-1).getZ());
                line(pointList.get(indices[6]-1).getX(),pointList.get(indices[6]-1).getY(),pointList.get(indices[6]-1).getZ(),pointList.get(indices[0]-1).getX(),pointList.get(indices[0]-1).getY(),pointList.get(indices[0]-1).getZ());
                //TODO: Придумать, как использовать написанный вручную line()
            }
        }
        scanner.close();
    }

    public void box(float x, float y, float z, float width, float height, float depth, Color color) {
        //Check
        drawPoint(x, y, z, color);
        drawPoint(x + width, y, z, color);

        //Check
        drawPoint(x + width, y, z, color);
        drawPoint(x + width, y, z + depth, color);

        //Check
        drawPoint(x + width, y, z + depth, color);
        drawPoint(x, y, z + depth, color);

        //Check
        drawPoint(x, y, z + depth, color);
        drawPoint(x, y, z, color);

        //Check
        drawPoint(x, y, z, color);
        drawPoint(x, y + height, z, color);

        //Check
        drawPoint(x, y + height, z, color);
        drawPoint(x + width, y + height, z, color);

        //Check
        drawPoint(x + width, y + height, z, color);
        drawPoint(x + width, y + height, z + depth, color);

        //Check
        drawPoint(x + width, y + height, z + depth, color);
        drawPoint(x, y + height, z + depth, color);

        //Check
        drawPoint(x, y + height, z + depth, color);
        drawPoint(x, y + height, z, color);

        //Check
        drawPoint(x + width, y, z, color);
        drawPoint(x + width, y + height, z, color);

        //Check
        drawPoint(x + width, y, z + depth, color);
        drawPoint(x + width, y + height, z + depth, color);

        //Check
        drawPoint(x, y, z + depth, color);
        drawPoint(x, y + height, z + depth, color);
    }


    public void drawShape(Color color, Point... points) {
        if (points.length <= 2)
            throw new IllegalArgumentException("Amount of points is less than 3. In this case you should use method line()");
        setColor(color);
        for (int i = 0; i < points.length - 1; i++) {
            line(points[i], points[i + 1], color);
        }
        line(points[points.length - 1], points[0], color);
    }

    public void setOrthographicProjectionX() {
        Matrix4 matrix = new Matrix4(new float[]
                {0f, 0f, 0f, 0f,
                        0f, 1f, 0f, 0f,
                        0f, 0f, 1f, 0f,
                        0f, 0f, 0f, 1f});
        getTransformMatrix().mul(matrix);
    }

    public void setOrthographicProjectionY() {
        Matrix4 matrix = new Matrix4(new float[]
                {1f, 0f, 0f, 0f,
                        0f, 0f, 0f, 0f,
                        0f, 0f, 1f, 0f,
                        0f, 0f, 0f, 1f});
        getTransformMatrix().mul(matrix);
    }

    public void setOrthographicProjectionZ() {
        Matrix4 matrix = new Matrix4(new float[]
                {1f, 0f, 0f, 0f,
                        0f, 1f, 0f, 0f,
                        0f, 0f, 0f, 0f,
                        0f, 0f, 0f, 1f});
        getTransformMatrix().mulLeft(matrix);
    }

    public void setOnePointProjection(float z) {
        float r = 1 / z;
        Matrix4 matrix = new Matrix4(new float[]
                {1f, 0f, 0f, 0f,
                        0f, 1f, 0f, 0f,
                        0f, 0f, 1f, r,
                        0f, 0f, 0f, 1f});
        getTransformMatrix().mul(matrix);
    }

    public void moveTo(Vector3 vector3) {
        //В каждой строке(не в столбце!!!) находится вектор
        Matrix4 moveMatrix = new Matrix4(new float[]
                {1f, 0f, 0f, 0f,
                        0f, 1f, 0f, 0f,
                        0f, 0f, 1f, 0f,
                        vector3.x, vector3.y, vector3.z, 1f});
        getTransformMatrix().mul(moveMatrix);
    }

    public void scaleCoords(float alpha, float beta, float delta) {
        Matrix4 scaleMatrix = new Matrix4(new float[]
                {alpha, 0f, 0f, 0f,
                        0f, beta, 0f, 0f,
                        0f, 0f, delta, 0f,
                        0f, 0f, 0f, 1f});
        getTransformMatrix().mul(scaleMatrix);
    }


    public void rotate(float aroundX, float aroundY, float aroundZ) {//TODO:Проверить
        rotateAroundZ(aroundY);
        rotateAroundX(aroundX);
        rotateAroundY(aroundZ);
    }

    public void rotateAroundY(float aroundY) {
        Matrix4 matrix = new Matrix4(new float[]
                {(float) Math.cos(aroundY), 0f, -(float) Math.sin(aroundY), 0f,
                        0f, 1f, 0f, 0f,
                        (float) Math.sin(aroundY), 0f, (float) Math.cos(aroundY), 0f,
                        0f, 0f, 0f, 1f});
        getTransformMatrix().mul(matrix);
    }

    public void rotateAroundX(float aroundX) {
        Matrix4 matrix = new Matrix4(new float[]
                {1f, 0f, 0f, 0f,
                        0, (float) Math.cos(aroundX), (float) Math.sin(aroundX), 0,
                        0f, (float) -Math.sin(aroundX), (float) Math.cos(aroundX), 0,
                        0f, 0f, 0f, 1f});
        getTransformMatrix().mul(matrix);
      //  System.out.println(getTransformMatrix().toString());

    }

    public void rotateAroundZ(float aroundZ) {

        Matrix4 matrix = new Matrix4(new float[]
                {(float) Math.cos(aroundZ), (float) Math.sin(aroundZ), 0f, 0f,
                        -(float) Math.sin(aroundZ), (float) Math.cos(aroundZ), 0f, 0f,
                        0f, 0f, 1f, 0f,
                        0f, 0f, 0f, 1f});
        getTransformMatrix().mul(matrix);

    }

    public void twistX(float angle) {
        twistX = true;
        this.angleTwistX = angle;
    }

    public void twistY(float angle) {
        twistY = true;
        this.angleTwistY = angle;
    }

    public void twistZ(float angle) {
        twistZ = true;
        this.angleTwistZ = angle;
    }

    public void bendX(float angle) {
        bendX = true;
        this.angleBendX = angle;
    }

    public void bendY(float angle) {
        bendY = true;
        this.angleBendY = angle;
        System.out.println(bendY);
    }

    public void bendZ(float angle) {
        bendZ = true;
        this.angleBendZ = angle;
    }

    /***
     * Отключает кручение
     */
    public void unstwistX() {
        twistX = false;
    }

    public void unstwistY() {
        twistY = false;
    }

    public void unstwistZ() {
        twistZ = false;
    }

    public void unbendX() {
        bendX = false;
    }

    public void unbendY() {
        bendY = false;
    }

    public void unbendZ() {
        bendZ = false;
    }

    public void scaleAngleTwistX(float scale) {
        if (twistX == true)
            this.angleTwistX = (angleTwistX * scale) % (2 * (float) Math.PI);
    }

    public void scaleAngleTwistY(float scale) {
        if (twistY == true)
            this.angleTwistY = (angleTwistY * scale) % (2 * (float) Math.PI);
    }

    public void scaleAngleTwistZ(float scale) {
        if (twistZ == true)
            this.angleTwistZ = (angleTwistZ * scale) % (2 * (float) Math.PI);
    }

    public void scaleAngleBendX(float scale) {
        if (bendX == true)
            this.angleBendX = (angleBendX * scale) % (2 * (float) Math.PI);
    }

    public void scaleAngleBendY(float scale) {
        if (bendY == true)
            this.angleBendY = (angleBendY * scale) % (2 * (float) Math.PI);
       // System.out.println(angleBendY);
    }

    public void scaleAngleBendZ(float scale) {
        if (bendZ == true)
            this.angleBendZ = (angleBendZ * scale) % (2 * (float) Math.PI);
    }

    public void line(Point p1, Point p2, Color color) {
        line(p1.getX(), p1.getY(), p1.getZ(), p2.getX(), p2.getY(), p2.getZ(), color);
    }

    public void line(float x1, float y1, float z1, float x2, float y2, float z2, Color color) {
        drawPoint(x1, y1, z1, color);
        drawPoint(x2, y2, z2, color);
    }

    private void drawPoint(Point point, Color color) {
        this.drawPoint(point.getX(), point.getY(), point.getZ(), color);
    }

    private void drawPoint(float x, float y, float z, Color color) {
        Vector3 vector = new Vector3(x, y, z);
        getRenderer().color(color);
        Matrix4 matrixTwistX = new Matrix4();
        Matrix4 matrixTwistY = new Matrix4();
        Matrix4 matrixTwistZ = new Matrix4();
        Matrix4 matrixBendX = new Matrix4();
        Matrix4 matrixBendY = new Matrix4();
        Matrix4 matrixBendZ = new Matrix4();

        if (twistX == true)
            matrixTwistX = new Matrix4(new float[]
                    {1f, 0f, 0f, 0f,
                            0, (float) Math.cos(angleTwistX * x), (float) Math.sin(angleTwistX * x), 0,
                            0f, (float) -Math.sin(angleTwistX * x), (float) Math.cos(angleTwistX * x), 0,
                            0f, 0f, 0f, 1f});
        if (twistY == true)
            matrixTwistY = new Matrix4(new float[]
                    {(float) Math.cos(angleTwistY * y), 0f, -(float) Math.sin(angleTwistY * y), 0f,
                            0f, 1f, 0f, 0f,
                            (float) Math.sin(angleTwistY * y), 0f, (float) Math.cos(angleTwistY * y), 0f,
                            0f, 0f, 0f, 1f});
        if (twistZ == true)
            matrixTwistZ = new Matrix4(new float[]
                    {(float) Math.cos(angleTwistZ * z), (float) Math.sin(angleTwistZ * z), 0f, 0f,
                            -(float) Math.sin(angleTwistZ * z), (float) Math.cos(angleTwistZ * z), 0f, 0f,
                            0f, 0f, 1f, 0f,
                            0f, 0f, 0f, 1f});

        if (bendX == true)
            matrixBendX = new Matrix4(new float[]
                    {1f, 0f, 0f, 0f,
                            0, (float) Math.cos(angleBendX * (Math.sqrt(y * y + z * z))), (float) Math.sin(angleBendX * Math.sqrt(y * y + z * z)), 0,
                            0f, (float) -Math.sin(angleBendX * Math.sqrt(y * y + z * z)), (float) Math.cos(angleBendX * Math.sqrt(y * y + z * z)), 0,
                            0f, 0f, 0f, 1f});
        if (bendY == true)
            matrixBendY = new Matrix4(new float[]
                    {(float) Math.cos(angleBendY * Math.sqrt(x * x + z * z)), 0f, -(float) Math.sin(angleBendY * Math.sqrt(x * x + z * z)), 0f,
                            0f, 1f, 0f, 0f,
                            (float) Math.sin(angleBendY * Math.sqrt(x * x + z * z)), 0f, (float) Math.cos(angleBendY * Math.sqrt(x * x + z * z)), 0f,
                            0f, 0f, 0f, 1f});
        if (bendZ == true)
            matrixBendZ = new Matrix4(new float[]
                    {(float) Math.cos(angleBendZ * Math.sqrt(x * x + y * y)), (float) Math.sin(angleBendZ * Math.sqrt(x * x + y * y)), 0f, 0f,
                            -(float) Math.sin(angleBendZ * Math.sqrt(x * x + y * y)), (float) Math.cos(angleBendZ * Math.sqrt(x * x + y * y)), 0f, 0f,
                            0f, 0f, 1f, 0f,
                            0f, 0f, 0f, 1f});

        matrixTwistX.mul(matrixTwistY).mul(matrixTwistZ).mul(matrixBendX).mul(matrixBendY).mul(matrixBendZ);
        vector.mul(matrixTwistX);
        getRenderer().vertex(vector.x, vector.y, vector.z);
    }

    @Override
    public void begin() {
        super.begin(ShapeType.Line);
    }

    /**
     * Костыль
     */
    @Override
    public void begin(ShapeType type) {
        if (!type.equals(ShapeType.Line))
            throw new IllegalArgumentException("We use only Line ShapeType for this class");

        super.begin(ShapeType.Line);
    }

}
