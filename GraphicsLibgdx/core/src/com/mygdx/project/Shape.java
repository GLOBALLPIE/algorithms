package com.mygdx.project;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * Created by GlobalPie on 14.10.2018.
 */

public class Shape extends Pixmap {
    /**
     * @param width  Ширина области
     * @param height Высота области
     */
    public Shape(int width, int height) {
        super(width, height, Format.RGB888);
        setColor(Color.WHITE);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                drawPixel(x, y);
            }
        }
        setColor(Color.BLACK);

    }

    public void drawFilledObj(String fileName, int scaleX, int scaleY) {
        Random random = new Random();
        String str;
        Point2D point;
        Scanner scanner;

        double[] coords = new double[3];//TODO: Можно оставить объявление
        int[] indices;
        ArrayList<Point2D> pointList = new ArrayList();


        try {
            scanner = new Scanner(new File(fileName));//useDelimiter("[#]+[\\w\\W]*\n?");
        } catch (FileNotFoundException f) {
            f.printStackTrace();
            System.exit(-1);//TODO: Нужно что-нибудь более изящное
            return;
        }
        while (scanner.hasNextLine()) {//Используем hasNextLine, а не hasNext
            str = scanner.nextLine();
            if (str.startsWith("v ")) {
                coords = Arrays.stream(str.substring(1).trim().split("[ ]+")).mapToDouble(coord -> Double.parseDouble(coord)).toArray();//TODO: Узнать, как работает split()
                point = new Point2D((int) (coords[0] * scaleX + Main.WIDTH / 2), ((int) (coords[1] * scaleY + Main.HEIGHT / 2)));
                pointList.add(point);
            }
            if (str.startsWith("f ")) {
                if (pointList.isEmpty()) break;
                indices = Arrays.stream(str.substring(1).trim().split("[/ ]+")).mapToInt(index -> Integer.parseInt(index)).toArray();
                drawFilledShape(new Color(random.nextInt()), pointList.get(indices[0] - 1), pointList.get(indices[3] - 1), pointList.get(indices[6] - 1));
            }
        }
        scanner.close();
    }

    public void drawObj(String fileName, int scaleX, int scaleY) {
        String str;
        Point2D point;
        Scanner scanner;

        double[] coords = new double[3];//TODO: Можно оставить объявление
        int[] indices;
        ArrayList<Point2D> pointList = new ArrayList();


        try {
            scanner = new Scanner(new File(fileName));//useDelimiter("[#]+[\\w\\W]*\n?");
        } catch (FileNotFoundException f) {
            f.printStackTrace();
            System.exit(-1);//TODO: Нужно что-нибудь более изящное
            return;
        }
        while (scanner.hasNextLine()) {//Используем hasNextLine, а не hasNext
            str = scanner.nextLine();
            if (str.startsWith("v ")) {
                coords = Arrays.stream(str.substring(1).trim().split("[ ]+")).mapToDouble(coord -> Double.parseDouble(coord)).toArray();//TODO: Узнать, как работает split()
                point = new Point2D((int) (coords[0] * scaleX + Main.WIDTH / 2), ((int) (coords[1] * scaleY + Main.HEIGHT / 2)));
                pointList.add(point);
            }
            if (str.startsWith("f ")) {
                if (pointList.isEmpty()) break;
                indices = Arrays.stream(str.substring(1).trim().split("[/ ]+")).mapToInt(index -> Integer.parseInt(index)).toArray();
                drawShape(Color.BLACK, pointList.get(indices[0] - 1), pointList.get(indices[3] - 1), pointList.get(indices[6] - 1));
            }
        }
        scanner.close();
    }

    /**
     * Алгоритм Вейлера-Азертона(по часовой стрелке)
     *
     * @param shape  Фигура(по часовой стрелке)
     * @param window Окно(по часовой стрелке)
     */
    public void drawVAShape(Point2D[] shape, Point2D[] window, Color color) {
        setColor(color);
        if (shape.length <= 2)
            throw new IllegalArgumentException("There are not enough points of a shape.");//TODO: Возможно, стоит это убрать
        if (window.length <= 2)
            throw new IllegalArgumentException("There are not enough points of a shape.");//TODO: Возможно, стоит это убрать
        //drawShape(color, window);
        List<Point2D> shapeList = new ArrayList<>(Arrays.asList(shape.clone()));
        List<Point2D> windowList = new ArrayList<>(Arrays.asList(window.clone()));//TODO:Узнать менее костыльный конструктор, чтобы не бросал UnsuportedException
        //Точки пересечения
        List<Point2D> pointList = new ArrayList<>();
        //Поиск пересечений
        for (int i = 0; i < shape.length; i++) {
            float t0, t1;
            for (int j = 0; j < window.length; j++) {
                t0 = linesIntersect(false, shape[i], shape[(i + 1) % shape.length], window[j], window[(j + 1) % window.length]);
                t1 = linesIntersect(false, window[j], window[(j + 1) % window.length], shape[i], shape[(i + 1) % shape.length]);

                if (Float.compare(t0, 0) > 0 && Float.compare(t1, 1) < 0 && Float.compare(t1, 0) > 0 && Float.compare(t1, 1) < 0) {

                    int x0 = Math.round(shape[i].getX() + t0 * (shape[(i + 1) % shape.length].getX() - shape[i].getX()));
                    int y0 = Math.round(shape[i].getY() + t0 * (shape[(i + 1) % shape.length].getY() - shape[i].getY()));
                    Point2D intersect = new Point2D(x0, y0);
                    pointList.add(intersect);
                    shapeList.add(shapeList.indexOf(shape[i]) + 1, intersect);
                    windowList.add(windowList.indexOf(window[j]) + 1, intersect);


                }
            }
        }
        pointList.forEach(System.out::println);
        System.out.println();
        if (pointList.isEmpty()) {
            for (int i = 0; i < shape.length; i++)
                if (!pointIsVisibleInWindow(true, shape[i], window)) return;
            drawShape(color, shape);
        }

        for (int i = 0; i < shape.length - 1; i++) {
            int finalI = i;
            Collections.sort(shapeList.subList(shapeList.indexOf(shape[i]) + 1, shapeList.indexOf(shape[(i + 1) % shape.length]) + 1), (p1, p2) -> Integer.compare(length(shape[finalI], p1), length(shape[finalI], p2)));
        }
        Collections.sort(shapeList.subList(shapeList.indexOf(shape[shape.length - 1]), shapeList.size()), (p1, p2) -> Integer.compare(length(shape[shape.length - 1], p1), length(shape[shape.length - 1], p2)));
        shapeList.forEach(System.out::println);
        System.out.println();

        Collections.sort(pointList, (p1, p2) -> shapeList.indexOf(p1) - shapeList.indexOf(p2));
        pointList.forEach(System.out::println);
        System.out.println();

        for (int j = 0; j < window.length - 1; j++) {
            int finalJ = j;
            Collections.sort(windowList.subList(windowList.indexOf(window[j]) + 1, windowList.indexOf(window[(j + 1) % window.length])), (p1, p2) -> (length(window[finalJ], p1) - length(window[finalJ], p2)));
        }
        Collections.sort(windowList.subList(windowList.indexOf(window[window.length - 1]), windowList.size()), (p1, p2) -> Integer.compare(length(window[window.length - 1], p1), length(window[window.length - 1], p2)));
        windowList.forEach(System.out::println);
        System.out.println();
        System.out.println(Integer.compare(length(new Point2D(400, 0), new Point2D(76, 182)), length(new Point2D(0, 255), new Point2D(46, 199))));

        int i0 = pointIsVisibleInWindow(false, shape[0], window) ? 0 : shapeList.indexOf(pointList.get(0));//TODO: Как-то неправильно
        int i = i0;
        System.out.println(i);
        //int i=0;
        main:
        do {
            drawBrezenhamLine(shapeList.get(i % shapeList.size()), shapeList.get((i + 1) % shapeList.size()), color);
            if (pointList.contains(shapeList.get((i + 1) % shapeList.size()))) {
                int j = windowList.indexOf(shapeList.get((i + 1) % shapeList.size()));
                while (true) {
                    drawBrezenhamLine(windowList.get(j % windowList.size()), windowList.get((j + 1) % windowList.size()), color);
                    if (pointList.contains(windowList.get((j + 1) % windowList.size()))) {
                        i = shapeList.indexOf(windowList.get((j + 1) % windowList.size()));
                        break;
                    } else j++;
                }
            } else i++;
        } while (i != i0);
    }

    /**
     * Алгоритм Сазерленда-Ходгмана(против часовой стрелки)
     *
     * @param currentShape Фигура
     * @param window       Окно(выпуклое)
     * @param color        Цвет
     */
    public void drawSHShape(Point2D[] currentShape, Point2D[] window, Color color) {
        if (currentShape.length <= 2)
            throw new IllegalArgumentException("There are not enough points of a shape.");//TODO: Возможно, стоит это убрать
        if (window.length <= 2)
            throw new IllegalArgumentException("There are not enough points of a shape.");//TODO: Возможно, стоит это убрать
        setColor(color);

        List<Point2D> shape = Arrays.asList(currentShape.clone());
        //Сортировка точек окна против часовой стрелки
        List<Point2D> shapeList = Arrays.asList(window.clone());
        float averageX = shapeList.stream().mapToInt(Point2D::getX).sum() / shapeList.size();
        float averageY = shapeList.stream().mapToInt(Point2D::getY).sum() / shapeList.size();
        shapeList.forEach((p) -> System.out.format("%s : atan2  = %f\n", p.toString(), Math.atan2(-p.getY() + averageY, -p.getX() + averageX)));
        Arrays.sort(window, (p1, p2) -> Double.compare(Math.atan2(-p1.getY() + averageY, -p1.getX() + averageX), Math.atan2(-p2.getY() + averageY, -p2.getX() + averageX)));
        drawShape(color, window);
        List<Point2D> pointList = new ArrayList();
        Point2D f;
        Point2D s;
        Point2D p;
        float t;

        for (int i = 0; i < window.length; i++) {
            f = new Point2D(shape.get(0));
            s = new Point2D(shape.get(0));
            if (pointIsVisible(s, window[i], window[(i + 1) % window.length]))
                pointList.add(0, s);
            for (int j = 1; j < shape.size(); j++) {
                p = new Point2D(shape.get(j));
                //Проверка на пересечение
                if (Float.compare(t = linesIntersect(false, s, p, window[i], window[(i + 1) % window.length]), 0) >= 0 && Float.compare(t, 1) <= 0)
                    pointList.add(new Point2D(Math.round(s.getX() + t * (p.getX() - s.getX())), Math.round(s.getY() + t * (p.getY() - s.getY()))));
                //Проверка вершины на нахождение в видимой области
                s = p;
                if (pointIsVisible(s, window[i], window[(i + 1) % window.length]))
                    pointList.add(s);

            }
            //Проверка последнего ребра
            if (Float.compare(t = linesIntersect(false, shape.get(shape.size() - 1), f, window[i], window[(i + 1) % window.length]), 0) >= 0 && Float.compare(t, 1) <= 0)
                pointList.add(new Point2D(Math.round(s.getX() + t * (f.getX() - s.getX())), Math.round(s.getY() + t * (f.getY() - s.getY()))));
            // f = new Point2D(pointList.get(0));
            // s = new Point2D(pointList.get(0));
            shape = pointList;
            pointList = new ArrayList();
        }
        drawShape(color, shape.toArray(new Point2D[shape.size()]));
    }

    /**
     * Рисует многоугольник
     *
     * @param color  Цвет
     * @param points Точки
     */
    public void drawShape(Color color, Point2D... points) {
        if (points.length == 0) return;//TODO: Проверить
        setColor(color);
        if (points.length <= 2) {//TODO: Возможно, стоит поставить drawBrezenhamLine на этот случай
            drawXiaoLine(points[points.length - 1].getX(), points[points.length - 1].getY(), points[0].getX(), points[0].getY(), color);
            return;
        }
        for (int i = 0; i < points.length - 1; i++) {
            drawBrezenhamLine(points[i].getX(), points[i].getY(), points[i + 1].getX(), points[i + 1].getY(), color);
        }
        drawBrezenhamLine(points[points.length - 1].getX(), points[points.length - 1].getY(), points[0].getX(), points[0].getY(), color);

    }

    /**
     * Алгоритм со списком реберных точек
     *
     * @param color  Цвет
     * @param points Точки
     */
    public void drawFilledShape(Color color, Point2D... points) {
        setColor(color);
        HashMap<Integer, List<Integer>> lists = new HashMap();
        int ymin = Arrays.asList(points).stream().mapToInt(Point::getY).min().getAsInt();
        int ymax = Arrays.asList(points).stream().mapToInt(Point::getY).max().getAsInt();
        //Инициализация
        for (int i = ymin; i <= ymax; i++) {
            lists.put(i, new ArrayList());
        }

        //Step 1
        for (int i = 0; i < points.length; i++) {
            //Ignore horizontal lines
            if (points[i].getY() == points[(i + 1) % points.length].getY()) {
                drawBrezenhamLine(points[i], points[(i + 1) % points.length], color);
                continue;
            }
            int previous = i == 0 ? (points.length - 1) : (i - 1);
            //Adding of a point with special conditions
            if (points[previous].getY() < points[i].getY() && points[i].getY() < points[(i + 1) % points.length].getY() || points[previous].getY() >= points[i].getY() && points[i].getY() >= points[(i + 1) % points.length].getY())
                lists.get(points[i].getY()).add(points[i].getX());

            int sgnX = points[i].getX() <= points[(i + 1) % points.length].getX() ? 1 : -1;
            int sgnY = points[i].getY() <= points[(i + 1) % points.length].getY() ? 1 : -1;
            float x = points[i].getX();
            int y = points[i].getY();
            float dx = Math.abs((Math.abs(points[(i + 1) % points.length].getX()) - Math.abs(x)) / (Math.abs(points[(i + 1) % points.length].getY()) - Math.abs(y))) * sgnX;
            while (y * sgnY <= points[(i + 1) % points.length].getY() * sgnY) {
                lists.get(y).add(Math.round(x));
                x += dx;
                y += sgnY;
            }
        }


        //Step 2
        for (int i = ymin; i <= ymax; i++) {
            Collections.sort(lists.get(i));
        }
        //Step 3
        for (int i = ymin; i <= ymax; i++)
            for (int j = 0; j < lists.get(i).size() / 2; j++)
                drawBrezenhamLine(lists.get(i).get(2 * j), i, lists.get(i).get(2 * j + 1), i, color);

    }

    /**
     * Построчный алгоритм с затравкой
     *
     * @param color  Цвет
     * @param points Точки
     */
    public void drawFilledShape2(Color color, Point2D... points) {//TODO: Нужно написать исключения, когда неправильно заданы точки для фигуры
        setColor(color);
        drawShape(color, points);
        Stack<Point2D> stack = new Stack();

        List<Point2D> pointList = Arrays.asList(points);
        stack.push(new Point2D(pointList.stream().mapToInt(Point::getX).sum() / pointList.size(), pointList.stream().mapToInt(Point::getY).sum() / pointList.size()));
        while (!stack.empty()) {
            Point2D point = stack.pop();
            drawPixel(point);
            int xmin = point.getX();
            Color current_color = new Color(getPixel(xmin - 1, -point.getY() + getHeight()));
            while (!current_color.equals(color) /*&& xmin>=0*/) {
                drawPixel(--xmin, -point.getY() + getHeight());
                // System.out.println(xmin);
                current_color = new Color(getPixel(xmin - 1, -point.getY() + getHeight()));
            }
            int xmax = point.getX();
            current_color = new Color(getPixel(xmax + 1, -point.getY() + getHeight()));
            while (!current_color.equals(color) /*&& xmax<=getWidth()*/) {
                drawPixel(++xmax, -point.getY() + getHeight());
                // System.out.println(xmax);
                current_color = new Color(getPixel(xmax + 1, -point.getY() + getHeight()));
            }

            //go down to add points to a stack
            boolean flagDown = true;
            for (int x = xmin; x < xmax; x++) {
                current_color = new Color(getPixel(x, -point.getY() + 1 + getHeight()));
                if (!current_color.equals(color) && current_color.equals(Color.WHITE)) {
                    if (flagDown) {
                        stack.push(new Point2D(x, point.getY() - 1));
                        flagDown = false;
                    }
                } else flagDown = true;
            }
            //go up to add points to a stack
            boolean flagUp = true;
            for (int x = xmin; x < xmax; x++) {
                current_color = new Color(getPixel(x, -point.getY() - 1 + getHeight()));
                if (!current_color.equals(color) && current_color.equals(Color.WHITE)) {
                    if (flagUp) {
                        stack.push(new Point2D(x, point.getY() + 1));
                        flagUp = false;
                    }
                } else flagUp = true;
            }
        }
    }

    /**
     * Алгоритм Лианга-Барски
     *
     * @param color  Цвет
     * @param x      Абсцисса точки прямоугольной области
     * @param y      Ордината точки прямоугольной области
     * @param width  Ширина прямоугльоной области
     * @param height Высота прямоугльоной области
     */
    public void drawLBLine(int x1, int y1, int x2, int y2, Color color, int x, int y, int width, int height) {
        setColor(color);
        drawShape(color, new Point2D(x, y), new Point2D(x + width, y), new Point2D(x + width, y + height), new Point2D(x, y + height));
        int[] p = new int[4];
        int[] q = new int[4];
        p[0] = -(x2 - x1);
        p[1] = (x2 - x1);
        p[2] = -(y2 - y1);
        p[3] = (y2 - y1);

        q[0] = x1 - x;
        q[1] = x + width - x1;
        q[2] = y1 - y;
        q[3] = y + height - y1;
        float u1 = 0;
        float u2 = 1;
        for (int i = 0; i < 4; i++) {
            if (p[i] == 0)
                if (q[i] < 0) return;
            if (p[i] < 0)
                u1 = Float.compare(u1, Math.max(0, (float) (q[i]) / p[i])) < 0 ? Math.max(0, (float) (q[i]) / p[i]) : u1;
            else
                u2 = Float.compare(u2, Math.min(1, (float) (q[i]) / p[i])) > 0 ? Math.min(1, (float) (q[i]) / p[i]) : u2;
        }
        if (Float.compare(u1, u2) > 0) return;
        drawBrezenhamLine(Math.round(x1 + u1 * (x2 - x1)), Math.round(y1 + u1 * (y2 - y1)), Math.round(x1 + u2 * (x2 - x1)), Math.round(y1 + u2 * (y2 - y1)), color);
    }

    /**
     * Алгоритм Кируса-Бека(обход верщин окна против часовой стрелки)
     *
     * @param color  Цвет линии и области
     * @param points Область видимости(Выпуклый многоугольник)
     */
    public void drawCBLine(int x1, int y1, int x2, int y2, Color color, Point2D... points) {
        List<Point2D> pointList = Arrays.asList(points);
        float averageX = pointList.stream().mapToInt(Point2D::getX).sum() / pointList.size();
        float averageY = pointList.stream().mapToInt(Point2D::getY).sum() / pointList.size();
        //  pointList.forEach((p) -> System.out.format("%s : atan2  = %f\n", p.toString(), Math.atan2(-p.getY() + averageY, -p.getX() + averageX)));
        pointList.sort((p1, p2) -> Double.compare(Math.atan2(-p1.getY() + averageY, -p1.getX() + averageX), Math.atan2(-p2.getY() + averageY, -p2.getX() + averageX)));
        pointList.forEach((p) -> System.out.format("%s : atan2  = %f\n", p.toString(), Math.atan2(-p.getY() + averageY, -p.getX() + averageX)));
        drawShape(color, points);

        //В итоге должно получиться два параметра, которые будут отвечать за точки пересечения отрезка с ребрами
        float td = 0;
        float tu = 1;
        //Направляющий вектор отрезка
        Point2D D = new Point2D(x2 - x1, y2 - y1);
        for (int i = 0; i < points.length; i++) {
            //Вектор с началом в вершине и концом в заданной точке для отрезка(первая точка (x1,y1))
            Point2D w = new Point2D(x1 - points[i].getX(), y1 - points[i].getY());
            //Нормаль, направленная внутрь
            Point2D n = new Point2D(points[i].getY() - points[(i + 1) % points.length].getY(), points[(i + 1) % points.length].getX() - points[i].getX());
            // Случай когда задана точка вместо отрезка, так как направляющий вектор D равен 0
            if (scalar(D, n) == 0)
                //Где находится точка относительно ребра(внутри области или снаружи)(если внутри, то нужно проверить, что такое верно и для других ребер)
                //   if(length(D,new Point2D(0,0))==0)
                if (scalar(w, n) < 0) return;
                else continue;

            //точка пересечения прямой ребра и заданной прямой
            float t = -(float) (scalar(w, n)) / scalar(D, n);

            //В таком случае ищем максимальный параметр из всех минимальных
            if (scalar(D, n) > 0)
                if (t > 1) continue;
                else {
                    //Новое значение минимального параметра
                    td = Math.max(t, td);
                    continue;
                }

            //(scalar(D, n) < 0) В таком случае ищем минимальный параметр из всех максимальных
            if (t < 0) continue;
            //Новое значение максимального параметра
            tu = Math.min(t, tu);
        }
        //Случай, когда весь отрезок находится за областью
        if (td > tu) return;
        System.out.println("tu: " + tu + "\n" + "td: " + td);
        //Отрисовываем полученный отрезок
        drawBrezenhamLine(Math.round(x1 + td * (x2 - x1)), Math.round(y1 + td * (y2 - y1)), Math.round(x1 + tu * (x2 - x1)), Math.round(y1 + tu * (y2 - y1)), color);

    }

    /**
     * Алгоритм Коэна-Сазерлэнда
     *
     * @param color Цвет
     */
    public void drawCSLine(int x1, int y1, int x2, int y2, int x0, int y0, int width, int height, Color color) {
        int code1 = code(x1, y1, x0, y0, width, height);
        int code2 = code(x2, y2, x0, y0, width, height);
        drawShape(color, new Point2D(x0, y0), new Point2D(x0 + width, y0), new Point2D(x0 + width, y0 + height), new Point2D(x0, y0 + height));
        if ((code1 | code2) == 0) {
            drawBrezenhamLine(x1, y1, x2, y2, color);
            return;
        } else if ((code1 & code2) != 0) return;
            //Change coords of points which are situated not into space of visibility
        else while ((code1 | code2) != 0) {
                int x, y, code;
                //Choose coords which don't have zero code
                if (code1 != 0) {
                    x = x1;
                    y = y1;
                    code = code1;
                } else {
                    x = x2;
                    y = y2;
                    code = code2;
                }
                switch (code) {
                    case 1:
                    case 5:
                    case 9:
                        x = x0;
                        y = Math.round(y1 + (float) (x - x1) / (x2 - x1) * (y2 - y1));
                        break;
                    case 2:
                    case 6:
                    case 10:
                        x = x0 + width;
                        y = Math.round(y1 + (float) (x - x1) / (x2 - x1) * (y2 - y1));
                        break;
                    case 4:
                        y = y0;
                        x = Math.round(x1 + (float) (y - y1) / (y2 - y1) * (x2 - x1));
                        break;
                    case 8:
                        y = y0 + height;
                        x = Math.round(x1 + (float) (y - y1) / (y2 - y1) * (x2 - x1));
                        break;
                }

                if (code1 != 0) {
                    x1 = x;
                    y1 = y;
                    code1 = code(x1, y1, x0, y0, width, height);
                } else {
                    x2 = x;
                    y2 = y;
                    code2 = code(x2, y2, x0, y0, width, height);
                }
            }
        drawBrezenhamLine(x1, y1, x2, y2, color);

    }

    /**
     * Простой ЦДА
     *
     * @param p1    Точка 1
     * @param p2    Точка 2
     * @param color Цвет
     * @see #drawSimpleDDALine(int, int, int, int, Color)
     */
    private void drawSimpleDDALine(Point2D p1, Point2D p2, Color color) {
        drawSimpleDDALine(p1.getX(), p1.getY(), p2.getX(), p2.getY(), color);
    }

    private void drawPixel(Point2D p) {
        drawPixel(p.getX(), -p.getY() + getHeight());
    }

    /**
     * Рисует окружность
     *
     * @param x     Абсцисса центра
     * @param y     Ордината центра
     * @param R     Радиус
     * @param color Цвет
     */
    public void drawSimpleCircle(int x, int y, int R, Color color) {
        setColor(color);
        for (int xi = 0; xi <= R / Math.sqrt(2); xi++) {
            int yi = (int) (Math.sqrt(R * R - xi * xi));
            drawPixel(xi + x, yi + -y + getHeight());
            drawPixel(yi + x, xi + -y + getHeight());
            drawPixel(yi + x, -xi + -y + getHeight());
            drawPixel(xi + x, -yi + -y + getHeight());
            drawPixel(-xi + x, -yi + -y + getHeight());
            drawPixel(-yi + x, -xi + -y + getHeight());
            drawPixel(-yi + x, xi + -y + getHeight());
            drawPixel(-xi + x, yi + -y + getHeight());
        }
    }

    /**
     * Рисует окружность алгоритмом Брезенхема
     *
     * @param x     Абсцисса центра
     * @param y     Ордината центра
     * @param R     Радиус
     * @param color Цвет
     */
    public void drawBrezenhamCircle(int x, int y, int R, Color color) {
        setColor(color);
        int xi = 0;
        int yi = R;
        int di = 2 * (1 - R);
        do {
            drawPixel(xi + x, yi + -y + getHeight());
            drawPixel(xi + x, -yi + -y + getHeight());
            drawPixel(-xi + x, yi + -y + getHeight());
            drawPixel(-xi + x, -yi + -y + getHeight());
            if (di < 0) {
                if (2 * (di + yi) - 1 <= 0) {
                    xi += 1;
                    di = di + 2 * xi + 1;
                } else {
                    xi += 1;
                    yi -= 1;
                    di = di + 2 * (xi - yi + 1);
                }
            } else if (di > 0) {
                if (2 * (di - xi) - 1 > 0) {
                    yi -= 1;
                    di = di - 2 * yi + 1;
                } else {
                    xi += 1;
                    yi -= 1;
                    di = di + 2 * (xi - yi + 1);
                }
            } else if (di == 0) {
                xi += 1;
                yi -= 1;
                di = di + 2 * (xi - yi + 1);
            }
        } while (yi >= 0);
    }

    /**
     * Рисует линию алгоритмом Сяолиня Ву
     *
     * @param color Цвет
     */
    public void drawXiaoLine(int x1, int y1, int x2, int y2, Color color) {
        setColor(color);
        if (x1 == x2 || y1 == y2) {
            drawSimpleDDALine(x1, y1, x2, y2, color);
            return;
        }

        boolean angle_behavior = Math.abs(y2 - y1) > Math.abs(x2 - x1);
        int sgnX = x1 <= x2 ? 1 : -1;
        int sgnY = y1 <= y2 ? 1 : -1;
        float dx = x2 - x1;
        float dy = y2 - y1;

        drawPixel(x1, -y1 + getHeight());
        if (!angle_behavior) {
            float gradient = Math.abs(dy / dx);
            float y = y1 + gradient * sgnY;
            for (int x = x1 + sgnX; x != x2 - sgnX + 1; x += sgnX) {

                setColor(color.r, color.g, color.b, 1 - (y - (int) y));
                drawPixel(x, -(int) y + getHeight());
                setColor(color.r, color.g, color.b, y - (int) y);
                drawPixel(x, -(int) y - 1 + getHeight());
                y += gradient * sgnY;
            }
        } else {
            float gradient = Math.abs(dx / dy);
            float x = x1 + gradient * sgnX;
            for (int y = y1 + sgnY; y != y2 - sgnY + 1; y += sgnY) {

                setColor(color.r, color.g, color.b, 1 - (x - (int) x));
                drawPixel((int) x, -y + getHeight());
                setColor(color.r, color.g, color.b, x - (int) x);
                drawPixel((int) x + 1, -y + getHeight());
                x += gradient * sgnX;
            }
        }
        setColor(color);
        drawPixel(x2, -y2 + getHeight());
    }

    /**
     * Рисует линию алгоритмом Брезенхема
     *
     * @param color Цвет
     * @see #drawBrezenhamLine(int, int, int, int, Color)
     */
    public void drawBrezenhamLine(Point2D p1, Point2D p2, Color color) {
        drawBrezenhamLine(p1.getX(), p1.getY(), p2.getX(), p2.getY(), color);
    }

    /**
     * Рисует линию алгоритмом Брезенхема
     *
     * @param color Цвет
     */
    public void drawBrezenhamLine(int x1, int y1, int x2, int y2, Color color) {// Done
        setColor(color);
        if (x1 == x2 && y1 == y2) {//TODO check it
            drawPixel(x1, -y1 + getHeight());
            return;
        }
        int x = x1;
        int y = y1;
        int dx = Math.abs(x2 - x1);
        int dy = Math.abs(y2 - y1);
        int s1 = (int) Math.signum(x2 - x1);
        int s2 = (int) Math.signum(y2 - y1);
        boolean change;
        if (dy > dx) {
            int temp = dx;
            dx = dy;
            dy = temp;
            change = true;
        } else {
            change = false;
        }
        int e = 2 * dy - dx;
        for (int i = 1; i <= dx; i++) {
            drawPixel(x, -y + getHeight());
            while (e >= 0) {
                if (change == true) x += s1;
                else y += s2;
                e -= 2 * dx;
            }
            if (change == true) y += s2;
            else x += s1;
            e += 2 * dy;
        }
    }

    /**
     * Рисует линию алгоритмом простого ЦДА
     *
     * @param color Цвет
     */
    public void drawSimpleDDALine(int x1, int y1, int x2, int y2, Color color) { //  Done
        setColor(color);

        int sgnX = x1 <= x2 ? 1 : -1;
        int sgnY = y1 <= y2 ? 1 : -1;
        int length;
        if (Math.abs(Math.abs(y1) - Math.abs(y2)) >= Math.abs(Math.abs(x1) - Math.abs(x2)))
            length = Math.abs(Math.abs(y1) - Math.abs(y2));
        else length = Math.abs(Math.abs(x1) - Math.abs(x2));


        float dx = (float) Math.abs(Math.abs(x1) - Math.abs(x2)) / (float) length * sgnX;
        float dy = (float) Math.abs(Math.abs(y1) - Math.abs(y2)) / (float) length * sgnY;
        float x = x1;
        float y = y1;

        for (int i = 1; i <= length; i++) {
            drawPixel(Math.round(x), -Math.round(y) + getHeight());
            x += dx;
            y += dy;
        }
    }

    /**
     * @return Код точки, состоящий из 4 битов, показыающий её расположение относительно заданной области видимости
     */
    private int code(int x, int y, int x0, int y0, int width, int height) {
        return (y > y0 + height ? 1 : 0) << 3 | (y < y0 ? 1 : 0) << 2 | (x > x0 + width ? 1 : 0) << 1 | (x < x0 ? 1 : 0);
    }


    private int scalar(Point2D p1, Point2D p2) {
        return scalar(p1.getX(), p1.getY(), p2.getX(), p2.getY());
    }

    /**
     * @return Скалярное произведение
     */
    private int scalar(int x1, int y1, int x2, int y2) {
        return x1 * x2 + y1 * y2;
    }

    /**
     * Проверяет, как лежит отрезок относительно границы окна(Видимая сторона окна зависит от порядка задания точек p1 и p2 в этом методе)
     *
     * @param s  Начальная точка отрезка
     * @param p  Конечная точка отрезка
     * @param p1 Точка границы окна
     * @param p2 Точка границы окна
     */
    public float linesIntersect(boolean hour, Point2D s, Point2D p, Point2D p1, Point2D p2) {
        if (s.equals(p) || p1.equals(p2)) throw new IllegalArgumentException("Something wrong with points");
        float td = 0;
        float tu = 1;
        float t;
        //Направляющий вектор отрезка
        Point2D D = new Point2D(p.getX() - s.getX(), p.getY() - s.getY());

        main:
        {
            //Вектор с началом в вершине и концом в заданной точке для отрезка(первая точка (x1,y1))
            Point2D w = new Point2D(s.getX() - p1.getX(), s.getY() - p1.getY());
            //Нормаль, направленная внутрь
            Point2D n = new Point2D(p1.getY() - p2.getY(), p2.getX() - p1.getX());
            if (scalar(D, n) == 0)
                t = 1.0f / 0;
            //точка пересечения прямой ребра и заданной прямой
            t = (hour == false ? -1 : 1) * (float) (scalar(w, n)) / scalar(D, n);
        }
        return t;
    }

    private boolean pointIsVisible(Point2D s, Point2D p1, Point2D p2) {
        if (p1.equals(p2)) throw new IllegalArgumentException("Something wrong with points");
        Point2D D = new Point2D(0, 0);

        Point2D w = new Point2D(s.getX() - p1.getX(), s.getY() - p1.getY());
        Point2D n = new Point2D(p1.getY() - p2.getY(), p2.getX() - p1.getX());
        if (scalar(D, n) == 0)
            //Где находится точка относительно ребра(внутри области или снаружи)(если внутри, то нужно проверить, что такое верно и для других ребер)
            if (scalar(w, n) < 0) return false;
        return true;
    }

    private boolean pointIsVisibleInWindow(boolean hour, Point2D s, Point2D... points) {
        drawShape(Color.BLACK, points);
        drawPixel(s);
        for (int i = 0; i < points.length; i++) {
            Point2D w = new Point2D(s.getX() - points[i].getX(), s.getY() - points[i].getY());
            //Нормаль, направленная внутрь
            Point2D n = new Point2D(points[i].getY() - points[(i + 1) % points.length].getY(), points[(i + 1) % points.length].getX() - points[i].getX());
            if ((hour ? -1 : 1) * scalar(w, n) < 0) return false;
            else continue;
        }
        return true;
    }

    private int length(Point2D p1, Point2D p2) {
        return (p2.getX() - p1.getX()) * (p2.getX() - p1.getX()) + (p2.getY() - p1.getY()) * (p2.getY() - p1.getY());
    }
}
