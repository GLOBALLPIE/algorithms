package com.mygdx.project;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Main extends Game {
    public static final float WIDTH = 800f;
    public static final float HEIGHT = 450f;
    public static Main main = new Main();
    SpriteBatch batch;

    private Main() {

    }

    public static Main getInstance() {
        return main;
    }

    @Override
    public void create() {
        batch = new SpriteBatch();
        //setScreen(new MainScreen(batch));
        setScreen(new Screen3D());
    }
}
