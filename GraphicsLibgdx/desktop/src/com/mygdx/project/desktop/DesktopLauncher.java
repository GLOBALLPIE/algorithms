package com.mygdx.project.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.project.Main;

public class DesktopLauncher {
    public static void main(String[] arg) {
        System.setProperty("user.name", "EnglishWords");
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = 800;
        config.height = 450;
        //config.fullscreen=true;
        config.title = "Graphics";
        new LwjglApplication(Main.getInstance(), config);

    }
}
